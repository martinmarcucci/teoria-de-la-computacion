/*
 * classNodo.hpp
 *
 *  Created on: 16/10/2013
 *      Author: guille
 */

#ifndef CLASSNODO_HPP_
#define CLASSNODO_HPP_

#include <iostream>
#include <stdio.h>
#include <cstdlib>

using namespace std;

class nodo{
      char dato;
      char tipo;
      nodo *proximo;

      public:
             nodo();
             nodo(int d,int t);
             char getDato();
             char getTipo();
             nodo *getProximo();
             void setDato(char d);
             void setTipo(char t);
             void insertarNodo(nodo *nuevoNodo);
};

#endif /* CLASSNODO_HPP_ */
