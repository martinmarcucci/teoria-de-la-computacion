/*
 * classLista.hpp
 *
 *  Created on: 16/10/2013
 *      Author: guille
 */

#ifndef CLASSLISTA_HPP_
#define CLASSLISTA_HPP_
#include "classNodo.hpp"

class lista {
	private:
            nodo *nlista;
	        int size;

	public:
    	lista();
	    lista(char d, char t);
	    int esVacia();
	    void insertar(char d, char t);
	    char buscar(char d);
	    void eliminar(int d);
	    ~lista();
};

#endif /* CLASSLISTA_HPP_ */
