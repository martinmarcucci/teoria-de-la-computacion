// class lista

#include "classLista.hpp"

lista::lista() {
	nlista = NULL;
	size = 0;
}

lista::lista(char d, char t) {
	nlista = new nodo(d, t);
	size++;
}

//a�ade valores a la lista en orden correcto

void lista::insertar(char d, char t) {
	cout << "insertando" << endl;
	nodo *newNodo = new nodo(d, t);
	nodo *prevNodo = NULL;
	nodo *currNodo = nlista;

	while (currNodo != NULL && d > currNodo->getDato()) {
		prevNodo = currNodo;
		currNodo = currNodo->getProximo();
	}
	if (prevNodo == NULL) {
		newNodo->insertarNodo(nlista);
		nlista = newNodo;
	} else {
		prevNodo->insertarNodo(newNodo);
		newNodo->insertarNodo(currNodo);
	}
	size++;
	cout << "Fin insert" << endl;
}

char lista::buscar(char d) {
	int i = 1;
	nodo *currLista = nlista;

	if (!esVacia()) {
		while (currLista != NULL && d != currLista->getDato()) {
			i++;
			currLista = currLista->getProximo();
		}
		if (i > size)
			return -1;
		else
			return (currLista->getTipo());
	} else
		return -2;
}

void lista::eliminar(int d) {
	nodo *currNodo = nlista;
	nodo *prevNodo = nlista;

	if (!esVacia()) {
		while (currNodo != NULL && d != currNodo->getDato()) {
			prevNodo = currNodo;
			currNodo = currNodo->getProximo();
		}
		if (currNodo)
			prevNodo->insertarNodo(currNodo->getProximo());
		delete (currNodo);
		size--;
	}
}

lista::~lista() {
	while (!esVacia())
		eliminar(0);
	size = 0;
}

int lista::esVacia() {

	if (nlista == NULL)
		return 1;
	else
		return 0;

}
