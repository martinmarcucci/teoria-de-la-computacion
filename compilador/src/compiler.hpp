#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include "classLista.hpp"

using namespace std;

class compiler {

private:
	char a;
	int posi;
	std::ifstream ifs;
	std::ofstream ofs;

	lista list_var;

	int linea;
	int n_car;

	int estado;

	int lexico();
	int definicion();
	int entero();
	int flotante();
	int caracter();
	int bloque();
	int sentencia();
	int leer();
	int escribir();
	int asignacion();
	int funcion(char tipo);
	int exp_f(char tipo);
	int term(char tipo);
	int exp_t(char tipo);
	int ope_m();
	int ope_s();
	int valor(char tipo);
	int variable(char check);
	int constante();
	int exp();

	int es_espacio(char c);

public:

	compiler(char* filename, char* output);
	int getestado(){ return estado;}

};
