/*
 * INICIO::=      M{ DEFINICION BLOQUE }
 * DEFINICION::=  ENTERO DEFINICION/ENTERO/
 * 				  FLOTANTE DEFINICION/FLOTANTE/
 * 				  CARACTER DEFINICION/CARACTER
 * ENTERO::=      INT VARIABLE;
 * FLOTANTE::=    FLOAT VARIABLE;
 * CARACTER::=    CHAR VARIABLE;
 * BLOQUE::=      SENTENCIA BLOQUE / SENTENCIA
 * SENTENCIA::=   LEER / ESCRIBIR / ASIGNACION
 * LEER::=        R VARIABLE ;
 * ESCRIBIR::=    W VARIABLE ;
 * ASIGNACION::=  VARIABLE = FUNC;
 *
 * FUNC::=    TERM EXP
 * EXP::=     OPE_S TERM EXP / VACIO
 * TERM::=     	  VALOR EXP_T
 * EXP_T::=  	  OPE_M VALOR EXP_T / VACIO
 *
 * VALOR::=       VARIABLE / CONSTANTE / ( FUNC )
 * CONSTANTE::=   0-9
 * VARIABLE::=    a-z
 * OPE_S::=       +  -
 * OPE_M::=       *  :  %
 *
 *
 std::ifstream ifs ("test.txt", std::ifstream::in);

 char c = ifs.get();

 while (ifs.good()) {
 std::cout << c;
 c = ifs.get();
 }

 ifs.close();
 */

#include "compiler.hpp"

compiler::compiler(char* filename, char* output) {
	ifs.open(filename, std::ifstream::in);
	ofs.open(output, std::ofstream::out);
	linea = 1;
	n_car = 0;

	if (lexico() || a != 'M')
		estado = 1;

	else if (lexico() || a != '{')
		estado = 1;

	else {
		ofs << ".CODE\n";
		if (definicion() != 0)
			estado = 1;
		if (bloque() != 0)
			estado = 1;
		else if (lexico() || a != '}')
			estado = 1;
		else {
			ofs << "END";
			ofs.close();
			estado = 0;
		}
	}
}

int compiler::definicion() {
	/*
	 * DEFINICION::=  ENTERO DEFINICION/ENTERO/
	 * 				  FLOTANTE DEFINICION/FLOTANTE/
	 * 				  CARACTER DEFINICION/CARACTER
	 *****************/
	int pos;
	pos = ifs.tellg();

	if (entero() == 0) {
		pos = ifs.tellg();
		if (definicion() != 0)
			ifs.seekg(pos);
		return 0;
	}
	ifs.seekg(pos);
	if (flotante() == 0) {
		pos = ifs.tellg();
		if (definicion() != 0)
			ifs.seekg(pos);
		return 0;
	}
	ifs.seekg(pos);
	if (caracter() == 0) {
		pos = ifs.tellg();
		if (definicion() != 0)
			ifs.seekg(pos);
		return 0;
	}
	return 1;     // no es sentencia, error.
}

int compiler::entero() {
	/* ENTERO::=      INT VARIABLE;*/
	char tmp;
	int pos;
	char valor;

	if (lexico())
		return 1;
	if (a != 'I')
		return 1;

	a = ifs.get();

	// salto todos los espacios y tabs
	do {
		pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque
		tmp = ifs.get();
	} while (es_espacio(tmp) == 2);

	ifs.seekg(pos);
	if (variable(0))
		return 1;

	valor = a;

	if (lexico() || a != ';')
		return 1;
	//Aca hay que guardarlo en la lista
	list_var.insertar(valor, 'I');

	return 0;
}

int compiler::flotante() {
	/* FLOTANTE::=    FLOAT VARIABLE;*/
	char tmp;
	int pos;
	char valor;

	if (lexico())
		return 1;
	if (a != 'F')
		return 1;

	// salto todos los espacios y tabs
	do {
		pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque
		tmp = ifs.get();
	} while (es_espacio(tmp) == 2);

	ifs.seekg(pos);

	if (variable(0))
		return 1;

	valor = a;

	if (lexico() || a != ';')
		return 1;
	//Aca hay que guardarlo en la lista
	list_var.insertar(valor, 'F');

	return 0;
}

int compiler::caracter() {
	/* CARACTER::=    CHAR VARIABLE;*/
	char tmp;
	int pos;
	char valor;

	if (lexico())
		return 1;
	if (a != 'C')
		return 1;

	// salto todos los espacios y tabs
	do {
		pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque
		tmp = ifs.get();
	} while (es_espacio(tmp) == 2);

	ifs.seekg(pos);
	if (variable(0))
		return 1;

	valor = a;

	if (lexico() || a != ';')
		return 1;
	//Aca hay que guardarlo en la lista
	list_var.insertar(valor, 'C');

	return 0;
}

int compiler::bloque() {
	int pos;

	if (sentencia() != 0)
		return 1;

	pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque

	if (bloque()) {
		ifs.seekg(pos);
	}

	return 0;
}

int compiler::sentencia() {
	int pos;
	pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque

	if (leer() == 0)
		return 0;

	ifs.seekg(pos);
	if (escribir() == 0)
		return 0;

	ifs.seekg(pos);
	if (asignacion() == 0) {
		ofs << "STORE\n";
		return 0;
	}
	return 1;     // no es sentencia, error.
}

int compiler::leer() {
	char var;
	if (lexico() || a != 'R')
		return 1;

	if (variable(1) != 0)
		return 1;
	var = a;
	if (lexico() || a != ';')
		return 1;
	ofs << "INPUT " << var << endl;
	return 0;
}

int compiler::escribir() {
	char var;
	if (lexico() || a != 'W')
		return 1;

	if (variable(1) != 0)
		return 1;
	var = a;
	if (lexico() || a != ';')
		return 1;
	ofs << "OUTPUT " << var << endl;
	return 0;
}

int compiler::asignacion() {
	/* ASIGNACION::=  VARIABLE = FUNC; */
	char tipo_asign;
	if (variable(1) != 0)
		return 1;

	tipo_asign = list_var.buscar(a);

	if (lexico() || a != '=')
		return 1;

	if (funcion(tipo_asign) != 0)
		return 1;

	if (lexico() || a != ';')
		return 1;

	return 0;
}

int compiler::funcion(char tipo) {
	/*
	 *  FUNC::=    TERM EXP
	 */
	if (term(tipo) != 0)
		return 1;

	if (exp_f(tipo) != 0)
		return 1;

	return 0;
}

int compiler::exp_f(char tipo) {
	/*
	 * EXP_F::=     OPE_S TERM EXP_F / VACIO
	 */
	int pos;
	char ope;
	pos = ifs.tellg(); // por si es vacio

	if (ope_s() == 0) {
		ope = a;
		if (term(tipo) == 0) {
			if (exp_f(tipo) == 0) {

				switch (ope) {
				case '+':
					ofs << "ADD\n";
					break;
				case '-':
					ofs << "NEG\nADD\n";
					break;
				}
				return 0;
			}
		}
		return 1;
	}
	ifs.seekg(pos);
	// Era vacio
	return 0;

}

int compiler::term(char tipo) {
	/*
	 * TERM::=     	  VALOR EXP_T
	 */
	if (valor(tipo) != 0)
		return 1;

	if (exp_t(tipo) != 0)
		return 1;

	return 0;
}

int compiler::exp_t(char tipo) {
	/*
	 *  EXP_T::=  	  OPE_M VALOR EXP_T / VACIO
	 */
	int pos;
	char ope;
	pos = ifs.tellg(); // por si es vacio

	if (ope_m() == 0) {
		ope = a;
		if (term(tipo) == 0) {
			if (exp_t(tipo) == 0) {

				switch (ope) {
				case '/':
					ofs << "DIV\n";
					break;
				case '%':
					ofs << "MOD\n";
					break;
				case '*':
					ofs << "MUL\n";
					break;
				}
				return 0;
			}
		}
		return 1;
	}
	ifs.seekg(pos);
	// Era vacio
	return 0;
}

int compiler::valor(char tipo) {
	int pos;
	char tmp;
	/* VALOR::=       VARIABLE / CONSTANTE / ( FUNC ) */
	pos = ifs.tellg(); // Guardo posicion actual del stream por si no sigue un bloque

	if (variable(1) == 0) {
		tmp = list_var.buscar(a);

		if ((tipo == 'I' && tmp == 'F') || (tipo == 'C' && tmp != 'C')) {
			cout << "error de tipo en asignacion" << endl;
			return 1;
		}

		ofs << "PUSHA " << a << endl << "LOAD\n";
		return 0;
	}

	ifs.seekg(pos);
	if (constante() == 0) {
		ofs << "PUSHC " << a << endl;
		return 0;
	}
	ifs.seekg(pos);

	// FUNC OPE VALOR

	if (lexico() == 0 && a == '(')
		if (funcion(tipo) == 0)
			if (lexico() == 0 && a == ')')
				return 0;

	return 1;
}

int compiler::variable(char check) {
	if (lexico() == 0 && (a >= 'a' && a <= 'z')) {
		if (check == 1) {
			//pertenece al lexico, vemos si esta definida;

			if (list_var.buscar(a) >= 0) {
				return 0;
			}
			cout << "variable " << a << " no Definida" << endl;
			return 1;
		} else {
			return 0;
		}
	}
	return 1;
}

int compiler::constante() {

	if (lexico() == 0 && (a >= '0' && a <= '9'))
		return 0;

	return 1;
}

int compiler::ope_m() {

	if (lexico() == 0) {
		switch (a) {
		case '*':
		case '/':
		case '%':
			return 0;
			break;
		}
	}
	return 1;
}

int compiler::ope_s() {

	if (lexico() == 0) {
		switch (a) {
		case '-':
		case '+':
			return 0;
			break;
		}
	}
	return 1;
}

int compiler::lexico() {
	do {
		if (!ifs.good())
			return 2;  // fin de archivo
		a = ifs.get();
		posi = ifs.tellg();
	} while (es_espacio(a));

	ifs.seekg(posi);
//Variables : Queda la primer letra en a para usarlo en el de sintaxis.
	if (a == 'I') {
		if (ifs.get() == 'N' && ifs.get() == 'T')
			return 0;
		else
			return 1;
	}
	if (a == 'F') {
		if (ifs.get() == 'L' && ifs.get() == 'O' && ifs.get() == 'A' && ifs.get() == 'T')
			return 0;
		else
			return 1;
	}
	if (a == 'C') {
		if (ifs.get() == 'H' && ifs.get() == 'A' && ifs.get() == 'R')
			return 0;
		else
			return 1;
	}
//Fin Variables
	if ((a >= 'a' && a <= '}' && a != '|') || (a >= '(' && a <= '9' && a != ',' && a != '.') || a == '%' || a == 'R'
			|| a == 'M' || a == 'W' || a == '=' || a == ';')
		return 0;
	else {
		cout << linea << ":" << n_car << "  Token " " no valido";
		return 1;
	}
}

int compiler::es_espacio(char c) {
	n_car++;
	switch (c) {
	case 0x0a:  // retorno de carro
		linea++;
		n_car = 0;
		return 1;
		break;
	case 0x0c:  // salto de linea
		return 1;
		break;
	case ' ':   // espacio
	case 0x09:	// tab
		return 2;
		break;
	default:
		return 0;
	}
}
