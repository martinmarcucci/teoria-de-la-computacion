//============================================================================
// Name        : AFD.cpp
// Author      : Martin Marcucci
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "clases/AutomataGeneral.hpp"
#include "clases/afd.hpp"
#include "clases/adp.hpp"
#include "clases/Atur.hpp"

#include <stdio.h>  /* defines FILENAME_MAX */
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

char cCurrentPath[FILENAME_MAX];

using namespace std;

char menu();

int main() {
	char key;
	string entrada;
	string file;
	AutomataGeneral *maquina;

	if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath))) {
		return 1;
	}
	// cCurrentPath[sizeof(cCurrentPath) - 1] = '\0';
	cout << "El directorio actual es: " <<  cCurrentPath << endl << endl;

	do {
		key = menu();
		if (key != 'x') {
			cout << "Ingrese la ruta del archivo: ";
			cin >> file;

			switch (key) {
			case '1':
				maquina = new afd((char *) file.c_str());
				break;
			case '2':
				maquina = new adp((char *) file.c_str());
				break;
			case '3':
				maquina = new Atur((char *) file.c_str());
				break;
			case '4':
				// maquina = new afd(file.c_str());
				break;
			}
			if (maquina->getError() != 0) {
				cout << "\t ERROR AL CARGAR LA MAQUINA"<<endl<<endl;
			}else{
				cout << "Para volver al menu, ingrese como entrada SALIR"<<endl;

				do {
					cout << "Ingrese nueva entrada:  ";
					cin >> entrada;

					if (entrada == "SALIR") {
						cout << "volviendo al menu" << endl;

					} else {
						maquina->set_entrada(entrada);
						if(maquina->hacer_transicion()!=-1){
						cout << "Estado actual " << maquina->ver_estado() << " sal: " << maquina->ver_salida() << endl
								<< endl;
						}
					}
				} while (entrada != "SALIR");
				delete maquina;
			}
		}
	} while (key != 'x');

	return 0;

}

char menu() {
	char key;
	cout << "Menu" << endl;
	cout << "\t1- Nuevo Automata finito" << endl;
	cout << "\t2- Nuevo Automata de pila" << endl;
	cout << "\t3- Nueva Maquina de Turing" << endl;
	cout << "\t4- Nuevo Analizador Sintactico" << endl << endl;
	cout << "\tx- salir" << endl;
	cout << "su eleccion: " << endl;

	cin >> key;
	return key;
}
