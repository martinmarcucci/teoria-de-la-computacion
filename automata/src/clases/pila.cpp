/*
 * pila.cpp
 *
 *  Created on: Sep 18, 2013
 *      Author: martin
 */
#include <iostream>
#include "pila.hpp"

pila::pila() {
	tope = NULL;
}

int pila::esta_vacia() {
	if (tope != NULL)
		return 0;
	return 1;
}

void pila::poner(int x) {
	nodo_pila *aux = new nodo_pila;
	aux->val = x;
	aux->next = tope;
	tope = aux;
}

int pila::sacar() {
	nodo_pila *aux;
	int x;
	x = tope->val;
	aux = tope;
	tope = aux->next;
	delete (aux);
	return x;
}
int pila::ver_tope() {
	if(tope == NULL){
		return -1;
	}
	return tope->val;
}
