#include "AutomataGeneral.hpp"
#include "pila.hpp"
#include <iostream>
#include <string>
#include <vector>

#ifndef _ADP_HPP
#define _ADP_HPP 1

using namespace std;

struct _transicion{
	int estado_sig;
	int apilo;	// n_alfa_entrada + 1 == no entrada
};

typedef struct _transicion transicion;

class adp: public AutomataGeneral {
protected:
	transicion ***ttransiciones;
	vector<string> alfa_pila;
	pila pil;

	int buscar_alfa_pila(string nombre);
public:
	adp(char * filename);
	int hacer_transicion();
	void ver_valor();
};

#endif
