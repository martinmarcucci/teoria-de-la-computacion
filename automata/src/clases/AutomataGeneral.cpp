#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include "tinyxml2.h"
#include "AutomataGeneral.hpp"

using namespace std;

AutomataGeneral::AutomataGeneral(char * filename) {
	tinyxml2::XMLDocument data;
	data.LoadFile(filename);

	tinyxml2::XMLNode* automata = data.FirstChild();
	error = 0;

	// Si es un archivo de ADP
	if (automata != NULL) {

		tinyxml2::XMLElement * padre = automata->FirstChildElement("Alfa_entrada");
		tinyxml2::XMLElement * hijo = padre->FirstChildElement("simbolo");
		string *str;
		estado *est_tmp;

		// lleno el alfabeto de entrada
		while (hijo != NULL) {
			str = new string(hijo->GetText());
			alfa_entrada.push_back(*str);
			cout << "agregada entrada " << alfa_entrada.back()  << endl;
			delete str;
			hijo = hijo->NextSiblingElement("simbolo");
		}

		padre = automata->FirstChildElement("Estados");
		hijo = padre->FirstChildElement("Estado");


		// lleno el alfabeto de entrada
		while (hijo != NULL) {
			str = new string(hijo->FirstChildElement("nombre")->GetText());
			est_tmp = new estado;
			est_tmp->nombre = *str;


			if (hijo->FirstChildElement("salida")->GetText()[0] == 'V') {
				est_tmp->salida = 1;
			} else {
				est_tmp->salida = 0;
			}

			estados.push_back(*est_tmp);
			delete str;
			delete est_tmp;

			cout << "agregado estado " << estados.back().nombre  << endl;
			hijo = hijo->NextSiblingElement("Estado");
		}
		estado_act = 0;
		entrada_act = -1;
	}else{
		error = 1;
	}
}

AutomataGeneral::~AutomataGeneral() {
}

int AutomataGeneral::ver_salida() {
	return estados[estado_act].salida;
}

string AutomataGeneral::ver_entrada() {
	return alfa_entrada[entrada_act];
}

string AutomataGeneral::ver_estado() {
	return estados[estado_act].nombre;
}

int AutomataGeneral::set_entrada(string e) {
	unsigned int alfa_largo = alfa_entrada.size();
	// Busco el el caracter de entrada en el vector
	for (unsigned int i = 0; i < alfa_largo; i++) {
		if (alfa_entrada[i] == e) {
			// si lo encontre, guardo el indice
			entrada_act = i;
			return 0;
		}
	}
	return 1;
}

int AutomataGeneral::buscar_estado(string nombre) {
	// Busco el estado en el vector
	for (unsigned int i = 0; i < estados.size(); i++) {
		if (estados[i].nombre == nombre) {
			// si lo encontre, guardo el indice
			return i;
		}
	}
	return -1;
}
int AutomataGeneral::buscar_alfa_entrada(string valor) {

	// Busco el el caracter de entrada en el vector
	for (unsigned int i = 0; i < alfa_entrada.size(); i++) {
		if (alfa_entrada[i] == valor) {
			// si lo encontre, guardo el indice
			return i;
		}
	}
	return -1;
}
