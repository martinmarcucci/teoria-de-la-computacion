#include "AutomataGeneral.hpp"

#ifndef _AFD_HPP
#define _AFD_HPP 1

using namespace std;

class afd: public AutomataGeneral {
protected:
	int **testados;

public:
	afd(char * filename);
	int hacer_transicion();
};

#endif
