#include "cinta.hpp"
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

cinta::cinta() {
	actual = new nodo;
	actual->ante = NULL;
	actual->next = NULL;
	actual->val = 0;
}
void cinta::mover(char lado) {
	if (lado == 'D') {
		if (actual->next == NULL) {
			actual->next = new nodo();
			actual->next->ante = actual;
			actual->next->next = NULL;
			actual->next->val = -1;
		}
		actual = actual->next;
	}
	if (lado == 'I') {
		if (actual->ante == NULL) {
			actual->ante = new nodo();
			actual->ante->ante = NULL;
			actual->ante->next = actual;
			actual->ante->val = -1;
		}
		actual = actual->ante;
	}
}

int cinta::leer() {
	return actual->val;
}
void cinta::grabar(int val) {
	actual->val = val;
}

vector<int> cinta::imprimir() {
	nodo *tmp = actual;
	vector<int> lista;

	while (tmp->ante != NULL) {
		tmp = tmp->ante;
	}

	while (tmp->next != NULL) {
		lista.push_back(tmp->val);
		tmp = tmp->next;
	}
	return lista;
}

void cinta::borrar() {
	while (actual->ante != NULL) {
		actual = actual->ante;
	}

	while (actual->next != NULL) {
		actual = actual->next;
		delete actual->ante;
		actual->ante = NULL;
	}
	actual->val = -1;
}
