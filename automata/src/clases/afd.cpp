#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include "tinyxml2.h"
#include "afd.hpp"

afd::afd(char * filename) :
		AutomataGeneral(filename) {

	tinyxml2::XMLDocument data;
	data.LoadFile(filename);

	tinyxml2::XMLElement* automata = data.FirstChildElement("AFD");

	// Si es un archivo de ADP
	if (automata != NULL) {

		testados = new int*[estados.size()];
		for (unsigned int i = 0; i < estados.size(); i++) {
			testados[i] = new int[alfa_entrada.size()];
		}

		tinyxml2::XMLElement *padre = automata->FirstChildElement(
				"Transiciones");
		tinyxml2::XMLElement *hijo = padre->FirstChildElement("Transicion");
		string *str;

		// lleno la tabla de transiciones
		int est_act, ent, est_sig;

		while (hijo != NULL) {

			// Cargo el estado actual
			str = new string(hijo->FirstChildElement("estado_act")->GetText());
			est_act = buscar_estado(*str);
			if (est_act < 0)
				error = 1;
			delete str;

			// Cargo entrada actual
			str = new string(hijo->FirstChildElement("entrada")->GetText());
			ent = buscar_alfa_entrada(*str);
			if (ent < 0)
				error = 1;
			delete str;

			// Cargo estado al que voy
			str = new string(hijo->FirstChildElement("estado_sig")->GetText());
			est_sig = buscar_estado(*str);
			if (est_sig < 0)
				error = 1;
			delete str;

			testados[est_act][ent] = est_sig;

			if (error == 0) {
				cout << "agregada transicion:\t";
				cout << estados[est_act].nombre << "\t";
				cout << alfa_entrada[ent] << "\t";
				cout << estados[est_sig].nombre << endl;
			}

			hijo = hijo->NextSiblingElement("Transicion");
		}
		estado_act = 0;
		entrada_act = -1;
	}
}

int afd::hacer_transicion() {

	estado_act = testados[estado_act][entrada_act];
	return estados[estado_act].salida;

}
