#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include "tinyxml2.h"
#include "adp.hpp"

adp::adp(char * filename) :
		AutomataGeneral(filename) {

	tinyxml2::XMLDocument data;
	data.LoadFile(filename);

	tinyxml2::XMLElement* automata = data.FirstChildElement("ADP");

	// Si es un archivo de ADP
	if (automata != NULL) {

		tinyxml2::XMLElement *padre = automata->FirstChildElement("Alfa_pila");
		tinyxml2::XMLElement *hijo = padre->FirstChildElement("simbolo");
		string *str;

		// lleno el alfabeto de pila
		while (hijo != NULL) {
			str = new string(hijo->GetText());
			alfa_pila.push_back(*str);
			delete str;

			cout << "agregada de pila " << alfa_pila.back() << endl;
			hijo = hijo->NextSiblingElement("simbolo");
		}
		// agrego el caracter de pila vacia
		alfa_pila.push_back("VACIO");

		// Agrego transicion instantanea
		alfa_entrada.push_back("NADA");

		ttransiciones = new transicion**[estados.size()];
		//[alfa_entrada.size()][alfa_pila.size()];

		// Inicializo la tabla a nulls
		for (unsigned int i = 0; i < estados.size(); i++) {
			ttransiciones[i] = new transicion*[alfa_entrada.size()];

			for (unsigned int j = 0; j < alfa_entrada.size(); j++) {
				ttransiciones[i][j] = new transicion[alfa_pila.size()];

				for (unsigned int k = 0; k < alfa_pila.size(); k++) {
					// estado_sig == -1 es que esta trancicion no esta declarada, por lo tanto es error;
					// el error es el ultimo estado.
					ttransiciones[i][j][k].estado_sig = -1;
				}
			}
		}
		padre = automata->FirstChildElement("Transiciones");
		hijo = padre->FirstChildElement("Transicion");

		// lleno la tabla de transiciones
		int est_act, ent, v_pila, apilo, est_sig;

		while (hijo != NULL) {

			// Cargo el estado actual
			str = new string(hijo->FirstChildElement("estado_act")->GetText());
			est_act = buscar_estado(*str);
			if (est_act < 0)
				error = 1;
			delete str;

			// Cargo entrada actual
			str = new string(hijo->FirstChildElement("entrada")->GetText());
			ent = buscar_alfa_entrada(*str);
			if (ent < 0)
				error = 1;
			delete str;

			// Cargo valor de pila actual
			tinyxml2::XMLElement *pilala = hijo->FirstChildElement("pila");
			str = new string(pilala->GetText());
			v_pila = buscar_alfa_pila(*str);
			// no es caracter de pila
			if (v_pila < 0)

				error = 1;
			delete str;

			// Cargo estado al que voy
			str = new string(hijo->FirstChildElement("estado_sig")->GetText());
			est_sig = buscar_estado(*str);
			if (est_sig < 0)
				error = 1;
			delete str;

			// Cargo que apilo
			str = new string(hijo->FirstChildElement("apilo")->GetText());
			apilo = buscar_alfa_pila(*str);
			// no es caracter de pila
			if (apilo < 0) {
				if (*str == "DESAPILO") // caracter de desapilo
					apilo = -1;
				else if (*str == "NADA") // caracter de desapilo
					apilo = -2;
				else
					error = 1;
			}
			delete str;

			ttransiciones[est_act][ent][v_pila].estado_sig = est_sig;
			ttransiciones[est_act][ent][v_pila].apilo = apilo;

			if (error == 0) {
				cout << "agregada transicion:\t";
				cout << estados[est_act].nombre << "\t";
				cout << alfa_entrada[ent] << "\t";
				cout << alfa_pila[v_pila] << "\t";
				cout << estados[est_sig].nombre << "\t";
				if (apilo >= 0)
					cout << alfa_pila[apilo] << endl;
				else if (apilo == -1)
					cout << "DESAPILO" << endl;
				else if (apilo == -2)
					cout << "NADA" << endl;
			}

			hijo = hijo->NextSiblingElement("Transicion");
		}
		estado_act = 0;
		entrada_act = -1;
	}
}

int adp::hacer_transicion() {

	transicion *temp;
	if (pil.esta_vacia())
		temp =
				&ttransiciones[estado_act][entrada_act][buscar_alfa_pila(
						"VACIO")];
	else
		temp = &ttransiciones[estado_act][entrada_act][pil.ver_tope()];

	// estado valido
	if (temp->estado_sig >= 0 && temp->estado_sig < (int) estados.size()) {
		estado_act = temp->estado_sig;

		if (temp->apilo >= 0)
			pil.poner(temp->apilo);
		else if (temp->apilo == -1)
			pil.sacar();

		// Me fijo si no hay transicion instantanea

		if (pil.esta_vacia()){
		temp =
				&ttransiciones[estado_act][buscar_alfa_entrada("NADA")][buscar_alfa_pila(
						"VACIO")];

			if (temp->estado_sig >= 0
					&& temp->estado_sig < (int) estados.size()) {
				estado_act = temp->estado_sig;
			}

		return estados[estado_act].salida;
		}
	} else if (temp->estado_sig == -1) { //estado -1 transicion no cargada, ir al error

		estado_act = estados.size() - 1;
		return 0;
	}
	return -1;
}

void adp::ver_valor() {
	cout << "Tope" << pil.ver_tope() << endl;

}

int adp::buscar_alfa_pila(string valor) {

// Busco el el caracter de entrada en el vector
	for (unsigned int i = 0; i < alfa_pila.size(); i++) {
		if (alfa_pila[i] == valor) {
			// si lo encontre, guardo el indice
			return i;
		}
	}
	return -1;
}
