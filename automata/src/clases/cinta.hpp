/*
 * cinta.hpp
 *
 *  Created on: Sep 18, 2013
 *      Author: martin
 */

#include <vector>
#include <iostream>
#include <vector>

using namespace std;

#ifndef CINTA_HPP_
#define CINTA_HPP_

struct nodo {
	nodo* ante;
	int val;
	nodo* next;
};

class cinta {
private:
	nodo * actual;
public:
	cinta();
	void mover(char lado);
	int leer();
	void grabar(int val);
	vector<int> imprimir();
	void borrar();
};

#endif /* PILA_HPP_ */
