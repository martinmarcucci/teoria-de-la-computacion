/*
 * pila.hpp
 *
 *  Created on: Sep 18, 2013
 *      Author: martin
 */

#ifndef PILA_HPP_
#define PILA_HPP_

struct nodo_pila {
	int val;
	nodo_pila* next;
};

class pila {
private:
	nodo_pila * tope;
public:
	pila();
	void poner(int x);
	int sacar();
	int ver_tope();
	int esta_vacia();
};

#endif /* PILA_HPP_ */
