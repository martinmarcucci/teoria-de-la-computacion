#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include "tinyxml2.h"
#include "Atur.hpp"

Atur::Atur(char* filename) :
		AutomataGeneral(filename) {
	a = 0;
	tinyxml2::XMLDocument data;
	data.LoadFile(filename);
	tinyxml2::XMLElement* automata = data.FirstChildElement("ATUR");
	if (automata != NULL) {
		for (unsigned int j = 0; j < alfa_entrada.size(); j++) {
			alfa_cinta.push_back(alfa_entrada[j]);
		}
		tinyxml2::XMLElement *padre = automata->FirstChildElement("Alfa_Cinta");
		tinyxml2::XMLElement *hijo = padre->FirstChildElement("simbolo");
		string *str;
		while (hijo != NULL) {
			str = new string(hijo->GetText());
			if (buscar_alfa_cinta(*str) == -1)
				alfa_cinta.push_back(*str);
			delete str;
			cout << "agregada de cinta" << alfa_cinta.back() << endl;
			hijo = hijo->NextSiblingElement("simbolo");
		}
		alfa_cinta.push_back("Blanco");
		alfa_cinta.push_back("Nada");
		ttransiciones = new transicion_turing*[estados.size()];
		for (unsigned int i = 0; i < estados.size(); i++) {
			ttransiciones[i] = new transicion_turing[alfa_cinta.size()];
			for (unsigned int k = 0; k < alfa_cinta.size(); k++) {
				// estado_sig == -1 es que esta trancicion no esta declarada, por lo tanto es error;
				// el error es el ultimo estado.
				ttransiciones[i][k].estado_sig = -1;
			}
		}
		padre = automata->FirstChildElement("Transiciones");
		hijo = padre->FirstChildElement("Transicion");

		// lleno la tabla de transiciones
		int est_act, v_cinta, escribo, est_sig;
		char movimiento;

		while (hijo != NULL) {
			// Cargo el estado actual
			str = new string(hijo->FirstChildElement("estado_act")->GetText());
			est_act = buscar_estado(*str);
			if (est_act < 0)
				error = 1;
			delete str;

			// Cargo valor de cinta actual
			tinyxml2::XMLElement *cintala = hijo->FirstChildElement("cinta");
			str = new string(cintala->GetText());
			v_cinta = buscar_alfa_cinta(*str);
			// no es caracter de cinta
			if (v_cinta < 0)

				error = 1;
			delete str;

			// Cargo estado al que voy
			str = new string(hijo->FirstChildElement("estado_sig")->GetText());
			est_sig = buscar_estado(*str);
			if (est_sig < 0)
				error = 1;
			delete str;

			// Cargo que escribo
			str = new string(hijo->FirstChildElement("escribo")->GetText());
			escribo = buscar_alfa_cinta(*str);
			// no es caracter de cinta
			if (escribo < 0) {
				error = 1;
			}
			delete str;

			str = new string(hijo->FirstChildElement("muevo")->GetText());
			movimiento = str->c_str()[0];
			if (movimiento != 'D' && movimiento != 'I' && movimiento != 'P')
				error = 1;
			delete str;

			ttransiciones[est_act][v_cinta].estado_sig = est_sig;
			ttransiciones[est_act][v_cinta].escribo = escribo;
			ttransiciones[est_act][v_cinta].movimiento = movimiento;

			if (error == 0) {
				cout << "agregada transicion:\t";
				cout << estados[est_act].nombre << "\t";
				cout << alfa_cinta[v_cinta] << "\t";
				cout << estados[est_sig].nombre << "\t";
				if (escribo >= 0)
					cout << alfa_cinta[escribo] << "\t";
				cout << movimiento << endl;
			}

			hijo = hijo->NextSiblingElement("Transicion");
		}
		estado_act = 0;
		entrada_act = -1;
	}
}

int Atur::buscar_alfa_cinta(string str) {
//Busco el el caracter de entrada en el vector
	for (unsigned int i = 0; i < alfa_cinta.size(); i++) {
		if (alfa_cinta[i] == str) {
			// si lo encontre, guardo el indice
			return i;
		}
	}
	return -1;
}

int Atur::hacer_transicion() {
	transicion_turing *temp;
//cint.leer()=
	if (a != 0) {
		int cont = 0;
		cint.mover('I');
		do {
			cont++;
			int x = cint.leer();
			if (x == -1)
				x = alfa_cinta.size() - 2;
			temp = &ttransiciones[estado_act][x];

			if ((unsigned int) temp->escribo != alfa_cinta.size() - 1)
				cint.grabar(temp->escribo);
			estado_act = temp->estado_sig;
			cint.mover(temp->movimiento);

			//return estados[estado_act].salida;
			vector<int> para_imprimir = cint.imprimir();
			cout << "Cinta: ";
			for (unsigned int actual = 0; actual < para_imprimir.size();
					actual++) {
				if (para_imprimir[actual] != -1)
					cout << alfa_cinta[para_imprimir[actual]];

			}
			cout << endl;
		} while (!estados[estado_act].salida || cont > 1000);
		a = 0;
		estado_act = 0;
		cint.borrar();
		return 0;
	} else
		return -1;
}

int Atur::set_entrada(string e) {
	unsigned int alfa_largo = alfa_cinta.size();
// Busco el el caracter de entrada en el vector
	for (unsigned int i = 0; i < alfa_largo; i++) {
		if (alfa_cinta[i] == e) {
			// si lo encontre, guardo el indice
			cint.grabar(i);
			cint.mover('D');
			return 0;
		}
	}
	if (e == "ok") {
		a = 1;
		return 0;
	}
	return 1;
}
