#include "AutomataGeneral.hpp"
#include "cinta.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include "tinyxml2.h"

#ifndef _Atur_HPP
#define _Atur_HPP 1

using namespace std;

struct _transicion_turing {
	int estado_sig;
	int escribo;
	char movimiento; 	// 'D' derecha y 'I' izquierda
};
typedef struct _transicion_turing transicion_turing;

class Atur: public AutomataGeneral {
private:
	char a;
protected:
	transicion_turing **ttransiciones;
	vector<string> alfa_cinta;
	cinta cint;

public:
	Atur(char* filename);
	int hacer_transicion();
	int buscar_alfa_cinta(string str);
	int set_entrada(string e);
};

#endif
