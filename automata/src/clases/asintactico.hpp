#include "AutomataGeneral.hpp"
#include "asintactico.hpp"
#include <iostream>
#include <string>
#include <vector>

#ifndef _ASINTACTICO_HPP
#define _ASINTACTICO_HPP 1

using namespace std;

class asintactico: public AutomataGeneral {
protected:
	int **testados;

public:
	asintactico(char * filename);
	int hacer_transicion();
	int cargar_tabla_estados();
};

#endif
