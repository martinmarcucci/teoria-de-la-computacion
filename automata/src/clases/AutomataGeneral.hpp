#include<iostream>
#include<vector>
#include<cstring>

using namespace std;

#ifndef _AUTOMATA_HPP
#define _AUTOMATA_HPP 1

typedef struct {
	string nombre;
	int salida;
} estado;

class AutomataGeneral {
protected:

	std::vector<string> alfa_entrada;
	std::vector<estado> estados;

	int estado_act;
	int entrada_act;
	int error;

	AutomataGeneral() {
	}

	int buscar_estado(string nombre);
	int buscar_alfa_entrada(string nombre);

public:
	virtual ~AutomataGeneral();
	AutomataGeneral(char * filename);

	virtual int hacer_transicion() = 0;

	virtual int set_entrada(string e);				// Setea la entrada
	int ver_salida();
	string ver_estado();
	string ver_entrada();
	int getError() {
		return error;
	}
};

#endif
